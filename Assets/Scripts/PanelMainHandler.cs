﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PanelMainHandler : MonoBehaviour
{
    public GridLetters gridLetter;
    public InputField fieldWidth;
    public InputField fieldHeight;
    public Button buttonGenerate;
    public Button buttonMix;

    private EventSystem eventSystem;

    void Start()
    {
        eventSystem = EventSystem.current;
    }

    void Update()
    {
        // Переход по Tab в следующее поле
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (eventSystem.currentSelectedGameObject == fieldWidth.gameObject)
                SetSelectedField(fieldHeight.gameObject);
            else if (eventSystem.currentSelectedGameObject == fieldHeight.gameObject)
                SetSelectedField(fieldWidth.gameObject);
        }
    }

    public void OnClickButtonGenerate()
    {
        bool checkOk = true;
        int w = 0;
        int h = 0;
        if (!CheckStringToInt(fieldWidth.text) || !int.TryParse(fieldWidth.text, out w))
        {
            fieldWidth.text = "";
            checkOk = false;
        }
        if (!CheckStringToInt(fieldHeight.text) || !int.TryParse(fieldHeight.text, out h))
        {
            fieldHeight.text = "";
            checkOk = false;
        }

        if (checkOk)
        {
            gridLetter.width = w;
            gridLetter.height = h;

            // Минимальное значение 2,
            // максимальное проверяется в Generate()
            if (w < 2)
            {
                fieldWidth.text = "";
                checkOk = false;
            }
            if (h < 2)
            {
                fieldHeight.text = "";
                checkOk = false;
            }

            if (checkOk)
                StartCoroutine(ClearAndGenerate());
        }
    }

    private IEnumerator ClearAndGenerate()
    {
        gridLetter.GridClear();
        yield return null;      // ждем следующий кадр, чтобы удалились старые go
        gridLetter.Generate();
    }

    public void OnClickButtonMix()
    {
        if (gridLetter.IsFull() && !gridLetter.IsMixing())
        {
            gridLetter.Mix();
            Text txt = buttonMix.GetComponentInChildren<Text>();
            txt.text = "Остановить";
        }
        else if (gridLetter.IsMixing())
        {
            gridLetter.MixStop();
            Text txt = buttonMix.GetComponentInChildren<Text>();
            txt.text = "Перемешать";
        }
    }

    public void OnMixEnd()
    {
        Text txt = buttonMix.GetComponentInChildren<Text>();
        txt.text = "Перемешать";
    }

    public void OnClickButtonAnim()
    {
        if (!gridLetter.IsMixing())
        {
            gridLetter.NextAnimation();
        }
    }

    private bool CheckStringToInt(string str)
    {
        str = str.Trim();
        str = str.Replace("0", "");
        str = str.Replace("1", "");
        str = str.Replace("2", "");
        str = str.Replace("3", "");
        str = str.Replace("4", "");
        str = str.Replace("5", "");
        str = str.Replace("6", "");
        str = str.Replace("7", "");
        str = str.Replace("8", "");
        str = str.Replace("9", "");
        return str.Length > 0 ? false : true;
    }

    public void SetWidth(int w)
    {
        fieldWidth.text = w.ToString();
    }

    public void SetHeight(int h)
    {
        fieldHeight.text = h.ToString();
    }

    /// <summary>
    /// Устанавливаем активное поле для ввода данных
    /// </summary>
    /// <param name="nextField"></param>
    private void SetSelectedField(GameObject nextField)
    {
        if (nextField == fieldHeight.gameObject)
        {
            fieldHeight.OnPointerClick(new PointerEventData(eventSystem));
            eventSystem.SetSelectedGameObject(fieldHeight.gameObject);
        }
        else if (nextField == fieldWidth.gameObject)
        {
            fieldWidth.OnPointerClick(new PointerEventData(eventSystem));
            eventSystem.SetSelectedGameObject(fieldWidth.gameObject);
        }
    }
}
