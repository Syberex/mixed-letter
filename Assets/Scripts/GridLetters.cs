﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GridLetters : MonoBehaviour
{
    public GridLayoutGroup gridLayoutGroup;
    public PanelMainHandler panelMain;
    public int width = 0;
    public int height = 0;
    public float timeMixing = 2f;
    public GameObject prefab;
    public float minSizeLetter = 27f;   // минимальный размер ячейки с символом
    public float maxSizeLetter = 197f;  // максимальный размер ячейки с символом
    public float minSizeSpan = 3f;      // размер промежутка между ячейками

    private bool isGenerate = false;
    private bool isMix = false;
    private bool isMixStep = false;
    private bool isAnimation2 = false;
    private RectTransform rtrLetter1;
    private RectTransform rtrLetter2;
    private RectTransform rectTransform;
    private Vector3 startPos1 = Vector3.zero;
    private Vector3 startPos2 = Vector3.zero;
    private Vector3 targetPos1 = Vector3.zero;
    private Vector3 targetPos2 = Vector3.zero;
    private int targetSiblingIndex1 = 0;
    private int targetSiblingIndex2 = 0;
    private float timer = 0f;
    private int counterMix = 0;
    private Quaternion rotInvisible = Quaternion.Euler(0f, 90f, 0f);
    private int animation2State = 0;

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    void Update()
    {
        if (isMixStep)
        {
            timer = timer + Time.deltaTime;
            if (!isAnimation2)
            {
                // Процесс анимации перемещения
                // (лерпим между двумя позициями по таймеру)
                if (timer < timeMixing)
                {
                    rtrLetter1.position = Vector3.Lerp(startPos1, targetPos1, timer / timeMixing);
                    rtrLetter2.position = Vector3.Lerp(startPos2, targetPos2, timer / timeMixing);
                }
                else
                {
                    rtrLetter1.position = targetPos1;
                    rtrLetter2.position = targetPos2;
                    MixEnd();
                }
            }
            else
            {
                // Процесс анимации с поворотом
                // (лерпим между двумя поворотами)
                if (timer < (timeMixing * 0.5f))
                {
                    Quaternion rotNext = Quaternion.Lerp(Quaternion.identity, rotInvisible, timer / (timeMixing * 0.5f));
                    rtrLetter1.localRotation = rotNext;
                    rtrLetter2.localRotation = rotNext;
                }
                else if (timer < timeMixing)
                {
                    if (animation2State == 0)
                    {
                        rtrLetter1.position = targetPos1;
                        rtrLetter2.position = targetPos2;
                        animation2State++;
                    }
                    else
                    {
                        Quaternion rotNext = Quaternion.Lerp(rotInvisible, Quaternion.identity, (timer - (timeMixing * 0.5f)) / (timeMixing * 0.5f));
                        rtrLetter1.localRotation = rotNext;
                        rtrLetter2.localRotation = rotNext;
                    }
                }
                else
                {
                    rtrLetter1.localRotation = Quaternion.identity;
                    rtrLetter2.localRotation = Quaternion.identity;
                    MixEnd();
                }
            }
        }
    }

    private void MixStart(Transform tr1, Transform tr2)
    {
        rtrLetter1 = tr1.GetComponent<RectTransform>();
        rtrLetter2 = tr2.GetComponent<RectTransform>();

        startPos1 = rtrLetter1.position;
        startPos2 = rtrLetter2.position;
        targetPos1 = rtrLetter2.position;
        targetPos2 = rtrLetter1.position;
        // Отключаем группировщик
        gridLayoutGroup.enabled = false;
        // Запоминаем позицию в иерархии
        targetSiblingIndex1 = rtrLetter2.GetSiblingIndex();
        targetSiblingIndex2 = rtrLetter1.GetSiblingIndex();
        // Меняем позицию в иерархии, чтобы рисовались поверх
        rtrLetter1.SetAsLastSibling();
        rtrLetter2.SetAsLastSibling();

        timer = 0f;
        animation2State = 0;
        isMixStep = true;
    }

    private void MixEnd()
    {
        isMixStep = false;
        // Устанавливаем новую позицию в иерархии
        // Сначала меньшую, чтобы не нарушить иерархию
        if (targetSiblingIndex1 < targetSiblingIndex2)
        {
            rtrLetter1.SetSiblingIndex(targetSiblingIndex1);
            rtrLetter2.SetSiblingIndex(targetSiblingIndex2);
        }
        else
        {
            rtrLetter2.SetSiblingIndex(targetSiblingIndex2);
            rtrLetter1.SetSiblingIndex(targetSiblingIndex1);
        }
        // Включаем группировщик
        gridLayoutGroup.enabled = true;
        // Если перемешивание закончено, сообщим главной панели
        if (!isMix)
            panelMain.OnMixEnd();
    }

    public void Mix()
    {
        if (!isMix && !isGenerate && IsFull())
        {
            isMix = true;
            counterMix = 0;
            StartCoroutine(Mix(timeMixing + 0.1f));
        }
    }

    private IEnumerator Mix(float waitTime)
    {
        while (isMix)
        {
            counterMix++;
            int rnd1 = Random.Range(1, width * height);
            int rnd2 = 0;
            while (rnd2 == 0 || rnd2 == rnd1)
            {
                rnd2 = Random.Range(1, width * height);
            }

            Transform tr1 = transform.GetChild(rnd1);
            Transform tr2 = transform.GetChild(rnd2);
            MixStart(tr1, tr2);

            if (counterMix < (width * height))
                yield return new WaitForSeconds(waitTime);
            else
                isMix = false;
        }
    }

    public void MixStop()
    {
        isMix = false;
    }

    public void GridClear()
    {
        if (transform.childCount > 0)
        {
            Transform trLetter;
            for (int i = (transform.childCount-1); i >= 0; i--)
            {
                trLetter = transform.GetChild(i);
                Destroy(trLetter.gameObject);
            }
        }
    }

    public void Generate()
    {
        if (isMix || IsFull())
        {
            Debug.LogWarning("Generate fail: " + (isMix ? "mixing! " : "") + (IsFull() ? "full!" : ""));
            return;
        }

        // Ресайзим под новые размеры
        // Получаем размер панели, в которую надо вписаться
        RectTransform rectTrPanel = transform.parent.GetComponent<RectTransform>();
        float maxSizeW = rectTrPanel.rect.width / width - minSizeSpan;
        if (maxSizeW > maxSizeLetter)
            maxSizeW = maxSizeLetter;
        float maxSizeH = rectTrPanel.rect.height / height - minSizeSpan;
        if (maxSizeH > maxSizeLetter)
            maxSizeH = maxSizeLetter;
        float size = Mathf.Min(maxSizeW, maxSizeH);
        if (size < minSizeLetter)
        {
            size = minSizeLetter;
            // Пересчитаем кол-во символов в ширину и высоту,
            // так как не убирается
            if ((size + minSizeSpan) * width > rectTrPanel.rect.width)
                width = Mathf.FloorToInt(rectTrPanel.rect.width / (size + minSizeSpan));
            if ((size + minSizeSpan) * height > rectTrPanel.rect.height)
                height = Mathf.FloorToInt(rectTrPanel.rect.height / (size + minSizeSpan));
            // Обновляем значения введенные пользователем
            panelMain.SetWidth(width);
            panelMain.SetHeight(height);
        }
        // От полученного size рассчитаем размер грида
        rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (size + minSizeSpan) * width);
        rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (size + minSizeSpan) * height);
        // Установим размер ячеек для группировщика
        gridLayoutGroup.cellSize = new Vector2(size, size);
        gridLayoutGroup.spacing = new Vector2(minSizeSpan, minSizeSpan);

        isGenerate = true;
        float timer1 = Mathf.Lerp(0.001f, 0.05f, (1000f - width * height) / 1000f);
        StartCoroutine(GenerateNext(timer1));
        //GenerateNext2();
    }

    private IEnumerator GenerateNext(float waitTime)
    {
        while (isGenerate)
        {
            if (transform.childCount < (width * height))
            {
                GameObject go = Instantiate(prefab, transform);
                Text txt = go.GetComponentInChildren<Text>();
                int n = Mathf.FloorToInt(Random.Range(65, 90));  // ASCII A-Z
                txt.text = "" + (char) n;
                go.name = go.name + " " + (char)n;
            }
            else
                isGenerate = false;

            yield return new WaitForSeconds(waitTime);
        }
    }

    private void GenerateNext2()
    {
        while (isGenerate)
        {
            if (transform.childCount < (width * height))
            {
                GameObject go = Instantiate(prefab, transform);
                Text txt = go.GetComponentInChildren<Text>();
                int n = Mathf.FloorToInt(Random.Range(65, 90));  // ASCII A-Z
                txt.text = "" + (char)n;
            }
            else
                isGenerate = false;
        }
    }

    public bool IsMixing()
    {
        return isMix;
    }

    public bool IsFull()
    {
        return transform.childCount > 0;
    }

    public bool IsGenerate()
    {
        return isGenerate;
    }

    public void NextAnimation()
    {
        isAnimation2 = !isAnimation2;
    }
}
